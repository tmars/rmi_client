package com.talipov.controllers;

import com.talipov.ClientConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by marsel on 23.03.17.
 */
@Controller
public class MainController {
    @Autowired
    private ClientConverter clientConverter;

    public ClientConverter getClientConverter() {
        return clientConverter;
    }

    public void setClientConverter(ClientConverter clientConverter) {
        this.clientConverter = clientConverter;
    }

    @GetMapping(value = "/home")
    public String home(Model model) {
        model.addAttribute("msg", clientConverter.getVoluteCourses().convert(100f));
        return "/home";
    }
}
