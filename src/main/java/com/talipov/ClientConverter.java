package com.talipov;

/**
 * Created by marsel on 23.03.17.
 */
public class ClientConverter {
    private VoluteCourses voluteCourses;

    public VoluteCourses getVoluteCourses() {
        return voluteCourses;
    }

    public void setVoluteCourses(VoluteCourses voluteCourses) {
        this.voluteCourses = voluteCourses;
    }
}
